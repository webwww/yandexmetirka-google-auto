package httpv1

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"yandex-ggle-auto/internal/entities"
	"yandex-ggle-auto/internal/usecase"
)

func InitMux(usecase *usecase.CounterUsercase) *http.ServeMux {
	s := http.NewServeMux()
	s.HandleFunc("/go/", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodGet {
			renderTemplate(writer)
			return
		}
		counter, err := validateFormData(request)
		switch errors.Unwrap(err) {
		case nil:
		case ErrRequiredFieldsEmpty:
			writer.WriteHeader(http.StatusBadRequest)
			_, _ = writer.Write([]byte(err.Error()))
			return
		default:
			fmt.Printf("httpv1/go/ err #1: %s\n", err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = usecase.Create(counter)
		if err != nil {
			fmt.Printf("httpv1/go/ err #2: %s\n", err)
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	return s
}

func validateFormData(request *http.Request) (*entities.NewCounter, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, fmt.Errorf("httpv1/go/ validateFormData err #1: %w\n", err)
	}
	var (
		geo       = request.FormValue("geo")
		link      = request.FormValue("link")
		offerName = request.FormValue("offer-name")
		pageName  = request.FormValue("page-name")
		promoType = request.FormValue("promo-type")
	)
	offerID, err := strconv.Atoi(request.FormValue("offer-id"))
	if err != nil {
		return nil, fmt.Errorf("httpv1/go/ validateFormData err #2: %w\n", err)
	}
	switch {
	case geo == "",
		link == "",
		offerName == "",
		pageName == "",
		promoType == "",
		offerID == 0:
		return nil, fmt.Errorf("%w", ErrRequiredFieldsEmpty)
	}

	return &entities.NewCounter{
		Geo:       geo,
		Link:      link,
		OfferName: offerName,
		PageName:  pageName,
		PromoType: promoType,
		OfferID:   offerID,
	}, nil
}
