package httpv1

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"

	"yandex-ggle-auto/internal/entities"
	"yandex-ggle-auto/internal/usecase"
	mock_usecase "yandex-ggle-auto/internal/usecase/mocks"
)

func TestInitMux(t *testing.T) {
	type mockBehavior func(g *mock_usecase.MockGoogleSheetAPI, y *mock_usecase.MockYandexMetrikaAPI, counter *entities.NewCounter)
	tests := []struct {
		name         string
		inputBody    string
		inputCounter *entities.NewCounter
		mockBehavior mockBehavior
		statusCode   int
	}{
		{
			name:      "OK Parse correctly",
			inputBody: "offer-id=12&offer-name=dawd&geo=dawd&promo-type=land&page-name=wad&link=dawd",
			mockBehavior: func(g *mock_usecase.MockGoogleSheetAPI, y *mock_usecase.MockYandexMetrikaAPI, counter *entities.NewCounter) {
				y.EXPECT().Create(counter).Return(int64(1), nil)
				g.EXPECT().Write(counter, int64(1)).Return(nil)
			},
			inputCounter: &entities.NewCounter{OfferID: 12, OfferName: "dawd", Geo: "dawd", PromoType: "land", PageName: "wad", Link: "dawd"},
			statusCode:   200,
		},
		{
			name:      "ERROR Letter in order-id",
			inputBody: "offer-id=12AAA&offer-name=dawd&geo=dawd&promo-type=land&page-name=wad&link=dawd",
			mockBehavior: func(g *mock_usecase.MockGoogleSheetAPI, y *mock_usecase.MockYandexMetrikaAPI, counter *entities.NewCounter) {
				// break before expecting behavior, so NO expected behavior is a success test
			},
			inputCounter: &entities.NewCounter{},
			statusCode:   500,
		},
		{
			name:      "ERROR No required param (link)",
			inputBody: "offer-id=12&offer-name=dawd&geo=dawd&promo-type=land&page-name=wad",
			mockBehavior: func(g *mock_usecase.MockGoogleSheetAPI, y *mock_usecase.MockYandexMetrikaAPI, counter *entities.NewCounter) {
				// break before expecting behavior, so NO expected behavior is a success test
			},
			inputCounter: &entities.NewCounter{},
			statusCode:   400,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			yandexAPI := mock_usecase.NewMockYandexMetrikaAPI(c)
			googleAPI := mock_usecase.NewMockGoogleSheetAPI(c)
			test.mockBehavior(googleAPI, yandexAPI, test.inputCounter)

			usecases := usecase.New(googleAPI, yandexAPI)
			handler := InitMux(usecases)

			s := httptest.NewServer(handler)
			defer s.Close()

			resp, er := http.Post(fmt.Sprintf("%s/go/", s.URL), "application/x-www-form-urlencoded", strings.NewReader(test.inputBody))
			if er != nil {
				fmt.Printf("err: %s", er.Error())
				return
			}
			if resp.StatusCode != test.statusCode {
				t.Errorf("status code error, want = %d, got = %d", test.statusCode, resp.StatusCode)
			}
		})
	}

}
