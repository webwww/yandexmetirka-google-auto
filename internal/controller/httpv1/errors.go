package httpv1

import "errors"

var (
	ErrRequiredFieldsEmpty = errors.New("required fields empty")
)
