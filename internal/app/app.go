package app

import (
	"log"
	"net/http"

	"yandex-ggle-auto/config"
	"yandex-ggle-auto/internal/controller/httpv1"
	"yandex-ggle-auto/internal/usecase"
	"yandex-ggle-auto/internal/usecase/google"
	"yandex-ggle-auto/internal/usecase/yandex"
	"yandex-ggle-auto/pkg"
)

func Run(config *config.Config) {
	googleAPIClient := pkg.BuildServiceFromFile("credentials.json")
	counterUsecase := usecase.New(google.New(config.GoogleSpreadSheetID, googleAPIClient), yandex.New(config.YandexMetrikaKey))

	err := http.ListenAndServe(":8080", httpv1.InitMux(counterUsecase))
	if err != nil {
		log.Fatal(err)
	}
}
