package usecase

import (
	"yandex-ggle-auto/internal/entities"
)

//go:generate mockgen -source=interfaces.go -destination=mocks/mock.go

type (
	GoogleSheetAPI interface {
		Write(*entities.NewCounter, int64) error
	}

	YandexMetrikaAPI interface {
		Create(*entities.NewCounter) (int64, error)
	}
)
