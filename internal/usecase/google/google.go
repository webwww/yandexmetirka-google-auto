package google

import (
	"fmt"

	"google.golang.org/api/sheets/v4"

	"yandex-ggle-auto/internal/entities"
)

type GoogleSheetAPI struct {
	spreadSheetID string
	service       *sheets.Service
}

const googleSheetRange = "A:H"
const valueInputOption = "RAW"

func New(secretKey string, service *sheets.Service) *GoogleSheetAPI {
	return &GoogleSheetAPI{spreadSheetID: secretKey, service: service}
}

func (g *GoogleSheetAPI) Write(counter *entities.NewCounter, counterID int64) error {
	var vr sheets.ValueRange
	vr.Values = append(vr.Values, []interface{}{
		counterID,
		counter.OfferID,
		counter.OfferName,
		counter.Geo,
		counter.PromoType,
		"Неизвестно",
		counter.PageName,
		counter.Link,
	})
	if g.service == nil {
		return fmt.Errorf("uc.GoogleSheetAPI.Write #1: service is not implemented")
	}
	_, err := g.service.Spreadsheets.Values.Append(g.spreadSheetID, googleSheetRange, &vr).ValueInputOption(valueInputOption).Do()
	if err != nil {
		return fmt.Errorf("uc.GoogleSheetAPI.Write #2: %w\n", err)
	}
	fmt.Println("Счетчик записан")
	return nil
}
