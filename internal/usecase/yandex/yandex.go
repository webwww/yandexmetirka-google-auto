package yandex

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/tidwall/gjson"

	"yandex-ggle-auto/internal/entities"
)

const yandexMetrikaAPIUrl = "https://api-metrika.yandex.net/management/v1/counters"

type YandexMetrikaAPI struct {
	secretKey string
}

func New(secretKey string) *YandexMetrikaAPI {
	return &YandexMetrikaAPI{secretKey: secretKey}
}

func (y *YandexMetrikaAPI) Create(counter *entities.NewCounter) (int64, error) {
	req, err := y.BuildRequest(counter)
	if err != nil {
		return 0, fmt.Errorf("uc.yandexMetrikaAPI.Create #1, err: %w", err)
	}

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		return 0, fmt.Errorf("uc.yandexMetrikaAPI.Create #2, err: %w", err)
	}
	if response.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("uc.yandexMetrikaAPI.Create #3, status code: %d", response.StatusCode)
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return 0, fmt.Errorf("uc.yandexMetrikaAPI.Create #4, err: %w", err)
	}
	counterID := gjson.Get(string(body), "counter.id")
	fmt.Printf("Создаем счетчик с ID: %s \n", counterID.String())
	return counterID.Int(), nil
}

func (y *YandexMetrikaAPI) BuildRequest(counter *entities.NewCounter) (*http.Request, error) {
	u, err := url.Parse(counter.Link)
	if err != nil {
		return nil, fmt.Errorf("uc.yandexMetrikaAPI.BuildRequest #1, err: %w", err)
	}
	counter.Link = u.Host + u.Path
	requestBody := fmt.Sprintf(y.getCounterJSON(), counter.OfferName, counter.PageName)

	req, err := http.NewRequest("POST", yandexMetrikaAPIUrl, strings.NewReader(requestBody))
	if err != nil {
		return nil, fmt.Errorf("uc.yandexMetrikaAPI.BuildRequest #2, err: %w", err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("%s %s", "OAuth", y.secretKey))
	req.Header.Set("Content-Type", "application/x-yametrika+json")

	return req, nil
}

func (y *YandexMetrikaAPI) getCounterJSON() string {
	return `{
		"counter" : {
			"name" :  "%s",
			"site2" : {
				"site" :  "%s"
			},
			"goals": [
				{
					"name": "Lead",
					"type": "form",
					"default_price": 0.0,
					"is_retargeting": 0,
					"prev_goal_id": 0,
					"conditions": []
				}
			],
					"webvisor": {
					"arch_enabled": 0,
					"arch_type": "none",
					"load_player_type": "proxy",
					"wv_version": 2,
					"allow_wv2": true,
					"wv_forms": 1
				}  ,
							"code_options": {
					"async": 1,
					"informer": {
						"enabled": 0,
						"type": "ext",
						"size": 3,
						"indicator": "pageviews",
						"color_start": "FFFFFFFF",
						"color_end": "EFEFEFFF",
						"color_text": 0,
						"color_arrow": 1
					},
					"visor": 1,
					"track_hash": 0,
					"xml_site": 0,
					"clickmap": 1,
					"in_one_line": 0,
					"ecommerce": 0,
					"alternative_cdn": 0,
					"ecommerce_object": "dataLayer"
				}
		}
	}`
}
