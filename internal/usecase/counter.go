package usecase

import (
	"yandex-ggle-auto/internal/entities"
)

type CounterUsercase struct {
	GoogleAPI GoogleSheetAPI
	YandexAPI YandexMetrikaAPI
}

func New(g GoogleSheetAPI, y YandexMetrikaAPI) *CounterUsercase {
	return &CounterUsercase{
		GoogleAPI: g,
		YandexAPI: y,
	}
}

func (uc *CounterUsercase) Create(counter *entities.NewCounter) error {
	counterID, err := uc.YandexAPI.Create(counter)
	if err != nil {
		return err
	}

	err = uc.GoogleAPI.Write(counter, counterID)
	if err != nil {
		return err
	}
	return nil
}
