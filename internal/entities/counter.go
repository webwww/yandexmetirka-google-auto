package entities

type NewCounter struct {
	Geo       string
	Link      string
	OfferID   int
	OfferName string
	PageName  string
	PromoType string
}
