package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Config struct {
	YandexMetrikaKey    string `json:"yandexMetrikaKey"`
	GoogleSpreadSheetID string `json:"googleSpreadSheetID"`
}

func Load(filename string) (*Config, error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		return nil, err
	}
	return config, nil
}
