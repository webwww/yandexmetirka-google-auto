package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func TestConfig(t *testing.T) {
	name := "../config.json"
	t.Run("all fields are set in config JSON file", func(t *testing.T) {
		jsonFile, err := os.Open(name)
		if err != nil {
			t.Errorf(err.Error())
			return
		}
		byteValue, err := ioutil.ReadAll(jsonFile)
		if err != nil {
			t.Errorf("TestConfig #1: err %s", err.Error())
		}
		configMap := make(map[string]interface{})
		err = json.Unmarshal(byteValue, &configMap)
		if err != nil {
			t.Errorf("TestConfig #2: err %s", err.Error())
			return
		}
		cfg, err := Load(name)
		if err != nil {
			t.Errorf("TestConfig #3: err %s", err.Error())
			return
		}
		var fieldTags []string
		val := reflect.ValueOf(cfg).Elem()
		for i := 0; i < val.NumField(); i++ {
			fieldTag, ok := val.Type().Field(i).Tag.Lookup("json")
			if !ok {
				t.Errorf("TestConfig #4: field in struct not found")
				return
			}
			fieldTags = append(fieldTags, fieldTag)
		}
		for _, field := range fieldTags {
			if _, ok := configMap[field]; !ok {
				t.Errorf("TestConfig #5: field in JSON config not found")
				return
			}
		}
	})
}
