package main

import (
	"log"

	"yandex-ggle-auto/config"
	"yandex-ggle-auto/internal/app"
)

func main() {
	cfg, err := config.Load("config.json")
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	app.Run(cfg)
}
